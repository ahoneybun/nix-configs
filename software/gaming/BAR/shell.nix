{ pkgs ? import <nixpkgs> {} }:
  pkgs.mkShell {
    buildInputs = with pkgs; [
       appimage-run
       openal
    ];
    shellHook = ''
       export LD_LIBRARY_PATH="${pkgs.openal}/lib:$LD_LIBRARY_PATH"
       appimage-run Beyond-All-Reason-1.2988.0.AppImage
    '';
}
