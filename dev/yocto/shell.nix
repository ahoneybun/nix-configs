{ pkgs ? import <nixpkgs> {} }:
  pkgs.mkShell {
    nativeBuildInputs = with pkgs; [ 
       chrpath
       cpio
       debianutils
       diffstat
       file
       gawk
       git
       glibcLocales
       gnat
       gnumake
       iputils
       lz4
       # Python 3.12 and packages for it
       python312
       python312Packages.jinja2
       python312Packages.gitpython
       python312Packages.pexpect
       python312Packages.pip
       python312Packages.subunit
       socat
       texinfo
       unzip
       wget
       xz
       zstd
       ];
}
