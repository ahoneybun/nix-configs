{ pkgs ? import <nixpkgs> {} }:

  pkgs.mkShell {
    packages = with pkgs; [ cargo rustc ];

    #inputsFrom = with pkgs; [ bat ];
    
    RUST_BACKTRACE = 1;

    shellHook = ''
       echo "Ready to make some rust!"
    '';
}
