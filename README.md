# nix-configs

This holds my .nix files for NixOS

## Main Nix files: (nix-configs)

These files are for the configuration, software that I use and unstable software that I use, note that some systems like the Pinebook Pro will use a custom configuration file.

- `flake.nix` : This is the main flake for system selection then that includes configuration, hardware-configuration (created during installation of NixOS), home-manager, disko and nixos-hardware (depending on the host)
- `configuration.nix` : This is the main file for the base system

## Home Manager file: (nix-configs/home.nix)

This file is for using with [Home Manager](https://nix-community.github.io/home-manager/index.html#sec-install-standalone).

- `home.nix` : This file is for settings for my user like Git name/email and other settings

## Partition Nix files: (nix-configs/partitions/)

These files are for the partition layouts that I use.

- `luks-btrfs-subvolumes.nix` : This is the main layout that I use for pretty much every system that I install NixOS on

## Host Nix files: (nix-configs/hosts/)

These files are for the systems themselves such as my custom desktop, Thinkpad X13s or my homelab.

| Hostname   | About | Role | Architecture    |
| ----------:| -----:| ----:| ---------------:|
| `shepard`  | System76 nebula49 | 🖥️ | `x86_64-linux`  |
| `edi`      | custom build for homelab stuff | ☁️ | `x86_64-linux`  |
| `garrus`   | System76 Lemur Pro (lemp13) | 💻 | `x86_64-linux`  |
| `drack`    | Thinkpad X13s | 💻 | `aarch64-linux` |
| `jaal`     | PineBook Pro | 💻 | `aarch64-linux` |
| `vetra`    | Raspberry Pi 4B | ☁️ | `aarch64-linux` |
| `peebee`   | PinePhone | 📱 | `aarch64-linux` |
| `lexi`     | OnePlus 6T | 📱 | `aarch64-linux` | 

### Desktop Nix files: (nix-configs/desktops/)

These files are for the desktops (DE or WM) that I use at times.

- `plasma.nix` : This file is for the desktop, login manager and other KDE applications
- `gnome.nix` : This file is for the desktop and login manager
- `pantheon.nix` : This file is for the desktop and login manager (this removes AppCenter)
- `sway.nix` : This file is for the Sway WM

## Web Nix files: (nix-configs/web/)

These files are for websites such as LAMP and NGINX. Most of these are no longer being used so they may not work with the latest changes.

- `lamp.nix` : This file is a WIP for LAMP setup. 
- `ahoneybun-net.nix` : This file is a basic setup for my website (ahoneybun.net).
- `hydra-ahoneybun-net.nix` : This file is a NGINX reverse proxy for my [Hydra](https://github.com/NixOS/hydra) server pointing to the localhost.
- `cloud-ahoneybun-net.nix` : This file is for Nextcloud.
- `rockymtnlug-org.nix` : This file is for [RMLUG](https://rockymountainlinuxfest.org).
- `tildecafe-com.nix` : This file is for [Tildecafe](https://tildecafe.com).
- `stoners-space.nix` : This file is for Mastodon on my stoners.space domain, simple changes can be made for a different domain.
- `nginx-owncast.nix` : This file is a NGINX reverse proxy for [Owncast](https://owncast.online) though it is not currently working.

## Development Nix files: (nix-configs/dev/)

These files are for either `nix-shell` (default nix) or `nix develop` (flakes):

- `rust` : This is grabs cargo, rustc and others from nixpkgs but also sets the RUST_BACKTRACE variable as 1 to enable it.

### Screenshots

![GNOME Installation](screenshots/nixos-gnome.png)

![Pantheon Installation](screenshots/nixos-pantheon.png)

![COSMIC Installation](screenshots/NixOS-COSMIC.png)
