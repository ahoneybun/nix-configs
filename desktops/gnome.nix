{ config, pkgs, ... }: 

{
  # GNOME
  services.xserver = {
    enable = true;
    displayManager.gdm.enable = true;
    desktopManager.gnome.enable = true;
  };

  # Add GNOME packages
  environment.systemPackages = (with pkgs; [
    dconf-editor
    gnome-tweaks
    gnomeExtensions.appindicator
    gedit
  ]);

  # Remove GNOME packages
  environment.gnome.excludePackages = (with pkgs; [
    epiphany # web browser
    geary
    gnome-software
    gnome-connections
    gnome-photos
    gnome-tour
  ]);

  # Services
  services.udev.packages = with pkgs; [ gnome-settings-daemon ];

}
