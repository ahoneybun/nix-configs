{ config, pkgs, ... }: 

{
    # Plasma
    services.displayManager.sddm.enable = true;
    services.desktopManager.plasma6.enable = true;

    # Install some packages
    environment.systemPackages = 
            with pkgs; 
            [
               kdePackages.dragon
               kdePackages.merkuro
#               kdePackages.neochat # disable due to insecure issue with olm
            ];
}
