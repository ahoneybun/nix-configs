{ config, pkgs, ... }:

{
  
  home.username = "aaronh";
  home.homeDirectory = "/home/aaronh";
  home.stateVersion = "24.05";

  home.packages = with pkgs; [
    bat
    bottom
    cargo
    cyme
    dua
    eza
    fd
    freshfetch
    gcc
    gnumake
    pkg-config
    rustc
  ];

  programs.bash = {
    enable = true;
  };

  programs.nushell = {
    enable = true;
    shellAliases = {
      cat = "bat";
      ls = "eza";
      lsusb = "cyme";
    };
  };

  programs.git = {
    enable = true;
    lfs.enable = true;
    userName = "Aaron Honeycutt";
    userEmail = "aaronhoneycutt@protonmail.com";
    aliases = {
       undo = "reset HEAD~1 --mixed";
       amend = "commit -a --amend";
       feature = "commit -m feat: -m new-feature";
       fix = "commit -m fix: -m issue#";
    };
    extraConfig = {
      core.editor = "hx";
      color = {
         ui = "auto";
      };
      color.status = {
         added = "green bold";
         changed = "yellow bold";
         untracked = "red bold";
      };
      push = {
         autoSetupRemote = "true";
      };
      init = {
          defaultBranch = "main";
      };
    };
  };

  programs.gh.enable = true;

  programs.gh-dash = {
    enable = true;
    settings = {
        prSections = [
          {
            title = "My Pull Requests";
            filters = "is:open author:@me";
          }
          {
            title = "My Review Requests";
            filters = "is:open review-requested:@me";
          }
          {
            title = "Assigned PRs";
            filters = "is:open assignee:@me";
          }
        ];
    };
  };

  programs.helix = {
    enable = true;
    settings = { 
       theme = "onedark";
    };
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

}
