{ config, pkgs, ... }: 

{
    # Name your host machine
    networking.hostName = "garrus"; 
    
    # System76
    environment.systemPackages = with pkgs; [
        system76-keyboard-configurator
        firmware-manager

        # COSMIC Utils
        cosmic-ext-tweaks
        cosmic-ext-forecast
        cosmic-ext-tasks
        cosmic-ext-calculator
        cosmic-ext-examine
        stellarshot
    ];

    # COSMIC
    services.desktopManager.cosmic.enable = true;
    services.displayManager.cosmic-greeter.enable = true;

}
