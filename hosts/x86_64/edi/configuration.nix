{ config, pkgs, lib, ... }:

{
  imports =
    [
        ./hardware-configuration.nix
    ];

  boot.loader = {
    systemd-boot.enable = true;
    efi.canTouchEfiVariables = true;
  };

## Disabled as the drives may not be there when I install the OS.

#  boot.swraid = {
#     enable = true;
#     mdadmConf = ''
#        ARRAY /dev/md/edi:DATA metadata=1.2 UUID=f0384881:84c12f70:3dcad802:653ccc30
#        MAILADDR root
#     '';
#  };

  boot.kernelParams = [ "console=tty0" ];
  boot.binfmt.emulatedSystems = [ "aarch64-linux" ];

## Disabled as the drives may not be there when I install the OS.

#  fileSystems = {
#     "/mnt/DATA" = {
#        device = "/dev/md127";
#     };
#  };

  nix.settings = {
     experimental-features = [ "nix-command" "flakes" ];
     extra-platforms = [ "aarch64-linux" ];
  };

  nix.buildMachines = [{
     hostName = "localhost";
     systems = ["x86_64-linux"
                "aarch64-linux"];
     supportedFeatures = [ "kvm" "nixos-test" "big-parallel" "benchmark" ];
     maxJobs = 8;
  }];

  nixpkgs.config.allowUnfree = true;

  # Define user accounts
  users.users.aaronh = {
    description = "Aaron Honeycutt";
    home = "/home/aaronh";
    extraGroups = [ "wheel" "networkmanager" "adm" "video" ];
    isNormalUser = true;
    hashedPassword = "$6$aAcbLtqiqzySifls$jdKMOQjoWITHD/dWNNZVUH/qNc6aoJ7v4zYofi0U7IJSVTbmOfChS3mzaJbp57AodjdPNKPrnrip8Nlh2Qanx.";
  };

  # Install some packages
  environment.systemPackages =
     with pkgs;
        [
           bottom
           busybox
           byobu
           cloudflared
           ffmpeg-full
           freshfetch
           git
           git-lfs
           tmux
           zola
        ];

  hardware.graphics = {
    enable = true;
  };

  networking.hostName = "edi";
  networking.firewall = {
    enable = true;
    # 11434 = Ollama
    # 25565 = Minecraft
    allowedTCPPorts = [ 80 443 2342 9091 11434 25565 ];
  };

  nixpkgs.config.permittedInsecurePackages = [
    "openssl-1.1.1w"
  ];

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
  services.openssh.settings = {
    PermitRootLogin = "no";
  };

  # Services
  services.hydra = {
    enable = true;
    hydraURL = "localhost:3000";
    buildMachinesFiles = [ "/etc/nix/machines" ];
    notificationSender = "hydra@localhost";
    useSubstitutes = true;
  };

  services.ollama = {
     enable = true;
     acceleration = "rocm";
     environmentVariables =
       {
         OLLAMA_HOST = "0.0.0.0";
       };
  };

  services.tailscale.enable = true;

## These are disabled currently as I am testing this.

  systemd.user.services."website-git-update" = {
    enable = false;
    description = "website-git-update runs git to pull latest website changes";
    serviceConfig = {
     ExecStart = "/run/current-system/sw/bin/git -C /mnt/DATA/Website/ahoneybun.net pull";
    };
    wantedBy = [ "default.target" ];
  };

  systemd.user.timers."website-git-update" = {
    wantedBy = [ "timers.target" ];
      timerConfig = {
        OnBootSec = "15m";
        OnUnitActiveSec = "15m";
      };
  };

  systemd.user.services."website-zola-build" = {
    enable = false;
    description = "rebuild zola website";
    serviceConfig = {
      ExecStart = "/run/current-system/sw/bin/zola -r /mnt/DATA/Website/ahoneybun.net build";
    };
    wantedBy = [ "default.target" ];
  };

  systemd.user.timers."website-zola-build" = {
     wantedBy = [ "timers.target" ];
       timerConfig = {
         OnBootSec = "30m";
         OnUnitActiveSec = "30m";
       };
  };

  # System
  system.stateVersion = "24.11";
  system.autoUpgrade.enable = true;

}