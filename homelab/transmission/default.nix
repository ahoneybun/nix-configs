{ config, pkgs, lib, ... }:

{
  services.transmission = {
     enable = true;
     openFirewall = true;
     settings = {
       download-dir = "/mnt/DATA/Downloads";
       rpc-bind-address = "0.0.0.0";
       rpc-host-whitelist-enabled = false;
       rpc-whitelist-enabled = false;
     };
  };
}