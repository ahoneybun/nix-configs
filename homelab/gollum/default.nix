{ config, pkgs, lib, ... }:

{
  services.gollum = {
    enable = true;
    no-edit = true;
    stateDir = "/mnt/DATA/Wiki";
  };
}