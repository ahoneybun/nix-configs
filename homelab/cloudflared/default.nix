{ config, pkgs, lib, ... }:

{
  services = {
    cloudflared = {
      enable = true;
      tunnels = {
        "{{Tunnel UUID}}" = {
          credentialsFile = "{{/path/to/UUID.json}}";
          default = "http_status:404";
          ingress = {
            "ahoneybun.net" = "http://localhost:80";
            "www.ahoneybun.net" = "http://localhost:80";
            "photos.ahoneybun.net" = "http://localhost:2342";
            "hydra.ahoneybun.net" = "http://localhost:3000";
            "git.ahoneybun.net" = "http://localhost:3001";
            "music.ahoneybun.net" = "http://localhost:4533";
            "wiki.ahoneybun.net" = "http://localhost:4567";
            "books.ahoneybun.net" = "http://localhost:8080";
            "audiobookshelf.ahoneybun.net" = "http://localhost:8000";
            "home.ahoneybun.net" = "http://localhost:8082";
            "bin.ahoneybun.net" = "http://localhost:8088";
            "videos.ahoneybun.net" = "http://localhost:8096";
            "status.ahoneybun.net" = "http://localhost:61208";
          };
        };
      };
    };
  };
}
