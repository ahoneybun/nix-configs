{ config, pkgs, lib, ... }:

{
  services.gatus = {
    enable = true;
    openFirewall = true;
    settings = {
      web.port = 61208;
      endpoints = [
        {
          name = "Personal Site";
          url = "https://ahoneybun.net";
          interval = "5m";
          conditions = [
            "[STATUS] == 200"
            "[RESPONSE_TIME] < 300"
          ];
        }
        {
          name = "Jellyfin";
          url = "https://videos.ahoneybun.net/health";
          interval = "5m";
          conditions = [
            "[STATUS] == 200"
            "[BODY] == Healthy"
            "[RESPONSE_TIME] < 300"
          ];
        }
        {
          name = "Navidrome";
          url = "https://music.ahoneybun.net";
          interval = "5m";
          conditions = [
            "[STATUS] == 200"
          ];
        }
        {
          name = "Forgejo";
          url = "https://git.ahoneybun.net/";
          interval = "5m";
          conditions = [
            "[STATUS] == 200"
          ];
        }
        {
          name = "PhotoPrism";
          url = "https://photos.ahoneybun.net";
          interval = "5m";
          conditions = [
            "[STATUS] == 200"
          ];
        }
        {
          name = "Audiobookself";
          url = "https://audiobookshelf.ahoneybun.net/";
          interval = "5m";
          conditions = [
            "[STATUS] == 200"
          ];
        }
      ];
    };
  };
}