{ config, pkgs, lib, ... }:

{
  services.komga = {
    enable = true;
    openFirewall = true;
  };
}