{ config, pkgs, lib, ... }:

{
  services.wastebin = {
     enable = true;
     settings.WASTEBIN_BASE_URL = "https://bin.ahoneybun.net";
  };
}