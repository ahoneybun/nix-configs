{ config, pkgs, lib, ... }:

{
  services.navidrome = {
    enable = true;
    user = "aaronh";
    group = "navidrome";
    openFirewall = true;
    settings = {
      Address = "0.0.0.0";
      MusicFolder = "/mnt/DATA/Media/Music";
    };
  };
}