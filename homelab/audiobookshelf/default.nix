{ config, pkgs, lib, ... }:

{
  services.audiobookshelf = {
    enable = true;
    openFirewall = true;
  };
}