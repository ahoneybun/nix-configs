{ config, pkgs, lib, ... }:

{
  services.nginx = {
    enable = true;
    virtualHosts."ahoneybun.net" = {
      root = "/mnt/DATA/Website/ahoneybun.net/public";
      locations."/" = {
        extraConfig = ''
          default_type text/html;
        '';
      };
      locations."~* \.atom\.xml$" = {
        extraConfig = ''
          types { } default_type "application/atom+xml; charset=utf-8";
        '';
      };
    };
  };
}