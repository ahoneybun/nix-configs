{
  services.homepage-dashboard = {
    enable = true;
    settings = {
      language = "en";
      title = "Honeycutt Homelab";
      #background = "https://w.wallhaven.cc/full/l8/wallhaven-l85z2l.jpg";
      color = "neutral";
      headerStyle = "boxed";
      layout = {
        media = { style = "row"; columns = 4; };
        infra = { style = "row"; columns = 2; };
        games = { style = "row"; columns = 1; };
      };
    };
    widgets = [
      { resources = { label = "system"; cpu = true; memory = true; }; }
      { resources = { label = "storage"; disk = [ "/mnt/DATA" ]; }; }
    ];
    services = [
      {
        media = [
          {
            Navidrome = {
              icon = "navidrome.png";
              href = "https://music.ahoneybun.net";
              description = "music player";
            };
          }
          {
            Jellyfin = {
              icon = "jellyfin.png";
              href = "https://videos.ahoneybun.net";
              description = "media player";
              widget = {
                type = "jellyfin";
                url = "https://videos.ahoneybun.net";
                key = "";
                enableNowPlaying = "false";
                enableBlocks = "true";
              };
            };
          }
          {
            Komga = {
              icon = "komga.png";
              href = "https://books.ahoneybun.net";
              description = "book reader";
              widget = {
                type = "komga";
                url = "https://books.ahoneybun.net";
                username = "";
                password = "";
              };
            };
          }
          {
            PhotoPrism = {
              icon = "photoprism.png";
              href = "https://photos.ahoneybun.net";
              description = "photo viewer";
              widget = {
                type = "photoprism";
                url = "https://photos.ahoneybun.net";
                username = "";
                password = "";
              };
            };
          }
          {
            Audiobookshelf = {
               icon = "audiobookshelf.png";
               href = "https://audiobookshelf.ahoneybun.net";
               description = "audiobook player";
               widget = {
                 type = "audiobookshelf";
                 url = "https://audiobookshelf.ahoneybun.net";
                 key = "";
               };
            };
          }
        ];
      }
      {
        infra = [
          {
            Forgejo = {
              icon = "forgejo.png";
              href = "https://git.ahoneybun.net";
              description = "a lightweight software forge, with a highlight on being completely free software";
            };
          }
          {
            Gollum = {
              icon = "gollum.png";
              href = "https://wiki.ahoneybun.net";
              description = "a simple, Git-powered wiki for many kinds of markup and content.";
            };
          }
        ];
      }
      {
        games = [
          {
            Minecraft = {
              icon = "minecraft.png";
              href = "IP:25565";
              description = "stack some blocks";
              widget = {
                type = "minecraft";
                url = "udp://IP:25565";
              };
            };
          }
        ];
      }
    ];
  };
}