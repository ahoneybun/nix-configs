{ config, pkgs, lib, ... }:

{
  services.minecraft-server = {
     enable = true;
     eula = true;
     openFirewall = true;
  };

}