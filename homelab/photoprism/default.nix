{ config, pkgs, lib, ... }:

{
  services.photoprism = {
     enable = true;
     port = 2342;
     originalsPath = "/mnt/DATA/Media/Photos";
     address = "0.0.0.0";
     settings = {
       PHOTOPRISM_ADMIN_USER = "admin";
       PHOTOPRISM_ADMIN_PASSWORD = "admin";
       PHOTOPRISM_DEFAULT_LOCALE = "en";
       PHOTOPRISM_DATABASE_DRIVER = "mysql";
       PHOTOPRISM_DATABASE_NAME = "photoprism";
       PHOTOPRISM_DATABASE_SERVER = "/var/run/mysqld/mysqld.sock";
       PHOTOPRISM_DATABASE_USER = "photoprism";
     };
  };

  # MySQL
  services.mysql = {
    enable = true;
    dataDir = "/var/lib/mysql";
    package = pkgs.mariadb;
    ensureDatabases = [ "photoprism" ];
    ensureUsers = [ {
      name = "photoprism";
      ensurePermissions = {
        "photoprism.*" = "ALL PRIVILEGES";
      };
    } ];
  };

}
